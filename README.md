# README #

This is an experience viewer for Tree of Savior. You may need to update it occasionally if the memory addresses change due to a new patch or something.

### Dependencies ###

You will need the .NET 4.5 framework to run it if you don't have it.

[http://www.microsoft.com/en-us/download/details.aspx?id=30653](.NET Framework 4.5)