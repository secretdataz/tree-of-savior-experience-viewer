﻿namespace TreeOfSaviorExperienceViewer
{
    partial class ExperienceViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExperienceViewerForm));
            this.currentBaseExperienceLabel = new System.Windows.Forms.Label();
            this.requiredBaseExperienceLabel = new System.Windows.Forms.Label();
            this.currentBaseExperiencePercentLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.experienceFromLastKillLabel = new System.Windows.Forms.Label();
            this.baseKillsTilNextLevelLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.experiencePercentFromLastKillLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.baseExperiencePerHourLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.hoursTilLevelLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // currentBaseExperienceLabel
            // 
            this.currentBaseExperienceLabel.AutoSize = true;
            this.currentBaseExperienceLabel.Location = new System.Drawing.Point(3, 13);
            this.currentBaseExperienceLabel.Name = "currentBaseExperienceLabel";
            this.currentBaseExperienceLabel.Size = new System.Drawing.Size(0, 13);
            this.currentBaseExperienceLabel.TabIndex = 0;
            // 
            // requiredBaseExperienceLabel
            // 
            this.requiredBaseExperienceLabel.AutoSize = true;
            this.requiredBaseExperienceLabel.Location = new System.Drawing.Point(71, 13);
            this.requiredBaseExperienceLabel.Name = "requiredBaseExperienceLabel";
            this.requiredBaseExperienceLabel.Size = new System.Drawing.Size(0, 13);
            this.requiredBaseExperienceLabel.TabIndex = 1;
            // 
            // currentBaseExperiencePercentLabel
            // 
            this.currentBaseExperiencePercentLabel.AutoSize = true;
            this.currentBaseExperiencePercentLabel.Location = new System.Drawing.Point(148, 13);
            this.currentBaseExperiencePercentLabel.Name = "currentBaseExperiencePercentLabel";
            this.currentBaseExperiencePercentLabel.Size = new System.Drawing.Size(0, 13);
            this.currentBaseExperiencePercentLabel.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.currentBaseExperiencePercentLabel, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.requiredBaseExperienceLabel, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.currentBaseExperienceLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.experienceFromLastKillLabel, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.baseKillsTilNextLevelLabel, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.experiencePercentFromLastKillLabel, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.baseExperiencePerHourLabel, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.hoursTilLevelLabel, 7, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(626, 56);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // experienceFromLastKillLabel
            // 
            this.experienceFromLastKillLabel.AutoSize = true;
            this.experienceFromLastKillLabel.Location = new System.Drawing.Point(206, 13);
            this.experienceFromLastKillLabel.Name = "experienceFromLastKillLabel";
            this.experienceFromLastKillLabel.Size = new System.Drawing.Size(0, 13);
            this.experienceFromLastKillLabel.TabIndex = 3;
            // 
            // baseKillsTilNextLevelLabel
            // 
            this.baseKillsTilNextLevelLabel.AutoSize = true;
            this.baseKillsTilNextLevelLabel.Location = new System.Drawing.Point(336, 13);
            this.baseKillsTilNextLevelLabel.Name = "baseKillsTilNextLevelLabel";
            this.baseKillsTilNextLevelLabel.Size = new System.Drawing.Size(0, 13);
            this.baseKillsTilNextLevelLabel.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Current Exp";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(71, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Required Exp";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(148, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Current %";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(206, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Last Kill Exp";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(336, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Kills TNL";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(276, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Last Kill %";
            // 
            // experiencePercentFromLastKillLabel
            // 
            this.experiencePercentFromLastKillLabel.AutoSize = true;
            this.experiencePercentFromLastKillLabel.Location = new System.Drawing.Point(276, 13);
            this.experiencePercentFromLastKillLabel.Name = "experiencePercentFromLastKillLabel";
            this.experiencePercentFromLastKillLabel.Size = new System.Drawing.Size(0, 13);
            this.experiencePercentFromLastKillLabel.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(391, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Exp/Hr";
            // 
            // baseExperiencePerHourLabel
            // 
            this.baseExperiencePerHourLabel.AutoSize = true;
            this.baseExperiencePerHourLabel.Location = new System.Drawing.Point(391, 13);
            this.baseExperiencePerHourLabel.Name = "baseExperiencePerHourLabel";
            this.baseExperiencePerHourLabel.Size = new System.Drawing.Size(0, 13);
            this.baseExperiencePerHourLabel.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(438, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Hr/Lv";
            // 
            // hoursTilLevelLabel
            // 
            this.hoursTilLevelLabel.AutoSize = true;
            this.hoursTilLevelLabel.Location = new System.Drawing.Point(438, 13);
            this.hoursTilLevelLabel.Name = "hoursTilLevelLabel";
            this.hoursTilLevelLabel.Size = new System.Drawing.Size(0, 13);
            this.hoursTilLevelLabel.TabIndex = 15;
            // 
            // ExperienceViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 50);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExperienceViewerForm";
            this.Text = "Tree of Savior Experience Viewer (by Excrulon)";
            this.TopMost = true;
            this.DoubleClick += new System.EventHandler(this.ToggleBorderWithDoubleClick);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label currentBaseExperienceLabel;
        private System.Windows.Forms.Label requiredBaseExperienceLabel;
        private System.Windows.Forms.Label currentBaseExperiencePercentLabel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label experienceFromLastKillLabel;
        private System.Windows.Forms.Label baseKillsTilNextLevelLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label experiencePercentFromLastKillLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label baseExperiencePerHourLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label hoursTilLevelLabel;
    }
}

